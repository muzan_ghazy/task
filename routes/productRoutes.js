const express = require('express')
const productControllers = require('../controllers/product')

const router = express.Router()

router.get('/products', productControllers.getProducts)

router.post('/product', productControllers.addProduct)

module.exports = router
