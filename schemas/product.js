const mongoose = require('mongoose')
const Schema = mongoose.Schema
let productSchema = new Schema(
  {
    title: {
      type: String,
    },
    desc: {
      type: String,
    },
    imgUrl: {
      type: String,
    },
  },
  {
    collection: 'products',
  }
)
module.exports = mongoose.model('Product', productSchema)
