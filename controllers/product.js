let productSchema = require('../schemas/product')

exports.getProducts = (req, res, next) => {
  productSchema.find().then((products) => res.send(products))
}
exports.addProduct = (req, res, next) => {
  const { title, desc, imgUrl } = req.body

  productSchema
    .create({ title, desc, imgUrl })
    .then((product) => res.res(product))
    .catch(next)
}
