export type TPayload<B, P> = {
  body?: B
  params?: P
}
