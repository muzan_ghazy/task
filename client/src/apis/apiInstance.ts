// import BASE_URL from "@/constants/domain";
import axios from 'axios'
import { BASE_URL } from './constants/domain'

const apiInstance = axios.create({ baseURL: BASE_URL.PUBLIC_BASE_URL })

export default apiInstance
