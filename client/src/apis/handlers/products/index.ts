import apiInstance from '../../apiInstance'
import ROUTES from '../../constants/routes'
import { TPayload } from '../../types'
import { IProduct } from './types'

const getProducts = async () => {
  const data = await apiInstance.get(ROUTES.PRODUCTS.GET_ALL)
  return data
}

const addProduct = async (payload: TPayload<IProduct, null>) => {
  const data = await apiInstance.post(ROUTES.PRODUCTS.ADD, payload.body)
  console.log(data)
  return data
}

export const productsApis = {
  getProducts,
  addProduct,
}
