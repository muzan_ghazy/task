export type IProduct = {
  _id: string
  title: string
  desc: string
  imgUrl: string
}
