const PRODUCTS = {
  GET_ALL: '/products',
  ADD: '/product',
}

const ROUTES = {
  PRODUCTS,
}

export default ROUTES
