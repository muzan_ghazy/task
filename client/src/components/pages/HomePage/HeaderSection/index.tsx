import { Box, Typography } from '@mui/material'
import { TweenMax, Power3 } from 'gsap'
import { useEffect, useRef } from 'react'
import { Parallax } from 'react-parallax'

function HeaderSection() {
  let text: unknown = useRef(null)
  let title: unknown = useRef(null)

  useEffect(() => {
    TweenMax.to(text ?? null, 2, {
      opacity: 1,
      y: -10,
      ease: Power3.easeOut,
    })
    TweenMax.to(title ?? null, 3, {
      opacity: 1,
      y: -30,
      ease: Power3.easeOut,
      delay: 0.2,
    })
  }, [])
  return (
    <Parallax
      strength={100}
      bgImage="/images/header.svg"
      blur={{ min: -15, max: 15 }}
    >
      <Box component="header" sx={HEADER_STYLES}>
        <Box>
          <Typography
            component="body"
            sx={TEXT_STYLES}
            color="primary"
            ref={(el) => {
              text = el
            }}
          >
            Lorem, Ipsum, dolor, sit, amet
          </Typography>
          <Typography
            component="h1"
            sx={TITLE_STYLES}
            color="primary"
            ref={(el) => {
              title = el
            }}
          >
            Lorem Ipsum
          </Typography>
        </Box>
      </Box>
    </Parallax>
  )
}

export default HeaderSection

const HEADER_STYLES = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  width: '100vw',
}

const TITLE_STYLES = {
  opacity: 0,
  textAlign: 'center',
  fontWeight: 500,
  fontSize: '8rem',
  '@media screen and (max-width:900px)': {
    fontSize: '5rem',
  },
  '@media screen and (max-width:600px)': {
    fontSize: '3rem',
  },
}

const TEXT_STYLES = {
  opacity: 0,
  textAlign: 'center',
  wordSpacing: '5px',
  fontSize: '1.7rem',

  '@media screen and (max-width:600px)': {
    fontSize: '1.2rem',
    paddingBottom: '20px',
  },
}
