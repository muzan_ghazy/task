/* eslint-disable @typescript-eslint/no-unused-vars */
import { Box, Typography, Grid, Divider } from '@mui/material'

function SectionTwo() {
  return (
    <Box component="section">
      <Grid container spacing={2} alignItems="end" flexDirection="row-reverse">
        <Grid item lg={6} xs={12}>
          <Box sx={TEXTS_STYLES}>
            <Box sx={{ width: '70%' }}>
              <Typography color="secondary" sx={MAIN_TITLE} data-aos="fade-up">
                Lorem ipsum <br /> & dolor sit amet
              </Typography>
              <Typography data-aos="fade-up" data-aos-delay="200">
                Phempora quod soluta placeat sapiente esse illo! Obcaecati
                laboriosam, aut eligendi impedit error at aperiam!
              </Typography>
              <Box
                sx={{
                  display: 'flex',
                  gap: '60px',
                  marginTop: '20px',
                }}
              >
                <Box data-aos="fade-right" data-aos-delay="300">
                  <Typography color="secondary" sx={SUB_TITLE}>
                    Lorem ipsum
                  </Typography>
                  <Typography>Lorem ipsum</Typography>
                </Box>
                <Divider orientation="vertical" flexItem />

                <Box data-aos="fade-right" data-aos-delay="400">
                  <Typography color="secondary" sx={SUB_TITLE}>
                    Lorem ipsum
                  </Typography>
                  <Typography>Lorem ipsum</Typography>
                </Box>
              </Box>
            </Box>
          </Box>
        </Grid>
        <Grid item lg={6} xs={12}>
          <Box sx={IMAGES_STYLES} data-aos="fade-right" data-aos-delay="100">
            <Box sx={CONTAINER_STYLES}>
              <Box
                component="img"
                src="/images/about3.svg"
                data-aos="fade-right"
                data-aos-delay="500"
              />
            </Box>
          </Box>
        </Grid>
      </Grid>
    </Box>
  )
}

export default SectionTwo

const MAIN_TITLE = {
  fontSize: '3rem',
  wordSpacing: '10px',
  '@media screen and (max-width:900px)': {
    fontSize: '2rem',
  },
  '@media screen and (max-width:600px)': {
    fontSize: '1.7rem',
  },
}
const SUB_TITLE = {
  fontSize: '1.5rem',
  wordSpacing: '10px',
  '@media screen and (max-width:900px)': {
    fontSize: '1.2rem',
  },
  '@media screen and (max-width:600px)': {
    fontSize: '1rem',
  },
}

const CONTAINER_STYLES = {
  // marginTop: '-200px',
  // position: 'relative',
  zIndex: 2,
  '.img-container2': {
    width: '100%',
    height: '500px',
    position: 'relative',
    overflow: 'hidden',
  },
}
const IMAGES_STYLES = {
  background: 'url(/images/about2.svg)',
  backgroundRepeat: 'no-repeat',
  padding: '60px 20px 0',
  marginTop: '-40px',
  textAlign: 'right',
  backgroundSize: 'cover',
  width: '100%',
  '@media screen and (max-width:1200px)': {
    marginTop: '0',
  },
}
const TEXTS_STYLES = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'end',
  height: 'fit-content',
}
