import { Box } from '@mui/material'
import SectionOne from './SectionOne'
import SectionTwo from './SectionTwo'

function AboutSection() {
  return (
    <Box>
      <SectionOne />
      <SectionTwo />
    </Box>
  )
}

export default AboutSection
