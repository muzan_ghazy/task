/* eslint-disable @typescript-eslint/no-unused-vars */
import { Box, Typography } from '@mui/material'
import LineUI from '../../../common/lineUI/LineUI'

function SectionOne() {
  return (
    <Box component="section" sx={{ marginTop: '-140px' }}>
      <Box sx={TOP_TEXT}>
        <Typography color="secondary" sx={{ width: '250px', height: '70px' }}>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        </Typography>
      </Box>
      <Box sx={{ position: 'relative', height: 'fit-content' }}>
        <Box sx={SIDE_TEXT}>Lorem ipsum dolor sit amet</Box>
        <Box sx={CONTAINER_TEXTS} data-aos="fade-up">
          <Typography component="h2" sx={TITLE_STYLES}>
            Lorem ipsum dolor sit amet, consectetur
          </Typography>
          <Typography component="body">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente
            dignissimos maiores non sint iure reprehenderit soluta.
          </Typography>

          <Typography
            component="h2"
            sx={{ ...TITLE_STYLES, margin: '20px 0 0' }}
          >
            Lorem ipsum
          </Typography>
          <Typography component="body">
            Lorem ipsum dolor sit amet consectetur.
          </Typography>
        </Box>
      </Box>
      <Box data-aos="fade-left">
        <Box
          sx={{
            width: '100%',
            display: 'flex',
            justifyContent: 'end',
            alignItems: 'end',
          }}
        >
          <Box sx={{ marginRight: '-90px' }}>
            <LineUI text="Lorem ipsum" />
          </Box>
          <Box sx={CONTAINER_STYLES} data-aos="fade-left">
            <Box component="img" src="/images/about1.svg" />
          </Box>
        </Box>
      </Box>
    </Box>
  )
}

export default SectionOne

const SIDE_TEXT = {
  transform: 'rotate(-90deg)',
  width: 'fit-content',
  position: 'absolute',
  bottom: '80px',
  '@media screen and (max-width:600px)': {
    left: '-70px',
  },
}

const TOP_TEXT = {
  width: '100%',
  display: 'flex',
  justifyContent: 'end',
  '@media screen and (max-width:600px)': {
    visibility: 'hidden',
  },
}
const CONTAINER_TEXTS = {
  background: 'url(/images/bg.svg)',
  width: '65%',
  margin: 'auto',
  padding: '10% 15%',
  textAlign: 'center',
  '@media screen and (max-width:500px)': {
    padding: '10% ',
  },
}

const TITLE_STYLES = {
  fontSize: '3rem',
  wordSpacing: '10px',
  lineHeight: 1.2,
  marginBottom: '10px',
  '@media screen and (max-width:900px)': {
    fontSize: '2rem',
  },
  '@media screen and (max-width:600px)': {
    fontSize: '1.7rem',
  },
}

const CONTAINER_STYLES = {
  marginTop: '-200px',
  position: 'relative',
  width: '440px',
  zIndex: 2,
  '@media screen and (max-width:1200px)': {
    margin: 'auto',
  },
  '@media screen and (max-width:600px)': {
    left: 0,
    width: 'calc(100% - 40px)',
  },
}
