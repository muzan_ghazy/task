import { Box } from '@mui/material'
import ProductList from './ProductList'

function ProductsSection() {
  return (
    <Box
      component="section"
      sx={{
        background: 'url(/images/bg.svg)',
        width: '100%',
        paddingY: '60px',
        marginY: '60px',
      }}
      data-aos="fade-up"
    >
      <ProductList />
    </Box>
  )
}

export default ProductsSection
