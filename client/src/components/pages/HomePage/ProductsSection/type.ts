export type ICard = {
  _id: string
  title: string
  desc: string
  imgUrl: string
}
