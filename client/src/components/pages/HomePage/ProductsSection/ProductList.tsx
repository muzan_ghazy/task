import { Grid, Container, Box, Skeleton } from '@mui/material'
import ProductCard from './ProductCard'
import { ICard } from './type'
import LineUI from '../../../common/lineUI/LineUI'
import { useQuery } from 'react-query'
import { productsApis } from '../../../../apis/handlers/products'

function ProductList() {
  const { isLoading, data, isError } = useQuery(['get-all-products'], () =>
    productsApis.getProducts()
  )

  if (isError) return <code>Error</code>
  return (
    <Container sx={{ position: 'relative' }}>
      <Box sx={{ position: 'absolute', left: '-15px', bottom: 0 }}>
        <LineUI text="Lorem Ipsum" />
      </Box>
      <Box>
        {isLoading ? (
          <Grid container spacing={3}>
            {[0, 1, 2].map((elm) => (
              <Grid key={elm} item md={4} xs={12}>
                <Skeleton variant="rectangular" width={300} height={300} />
              </Grid>
            ))}
          </Grid>
        ) : (
          <Grid container spacing={3}>
            {data?.data.map((item: ICard, index: number) => (
              <Grid
                key={item._id}
                item
                md={4}
                xs={12}
                data-aos="fade-left"
                data-aos-delay={(index + 3) * 100}
              >
                <ProductCard
                  _id={item._id}
                  title={item.title}
                  desc={item.desc}
                  imgUrl={item.imgUrl}
                />
              </Grid>
            ))}
          </Grid>
        )}
      </Box>
    </Container>
  )
}

export default ProductList
