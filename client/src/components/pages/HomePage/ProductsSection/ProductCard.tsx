import { Box, Typography } from '@mui/material'
import { ICard } from './type'

function ProductCard({ title, desc, imgUrl }: ICard) {
  return (
    <Box sx={CARD_STYLES}>
      <Box component="img" src={imgUrl} alt={title} sx={IMAGE_STYLE} />
      <Box sx={TEXT_BOX_STYLES}>
        <Typography component="h2">{title}</Typography>
        <Typography component="body"> {desc}</Typography>
      </Box>
    </Box>
  )
}

export default ProductCard

const CARD_STYLES = {
  padding: '10px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  position: 'relative',
  maxWidth: '300px',
  width: '100%',
  margin: 'auto',
  height: '300px',
  overflow: 'hidden',
  cursor: 'pointer',
  '&:hover': {
    img: {
      filter: 'brightness(0.5)',
    },
  },
}

const TEXT_BOX_STYLES = {
  padding: '10px 20px',
  background: '#fff',
  position: 'relative',
  zIndex: 3,
  textAlign: 'center',
  h2: {
    fontWeight: 600,
    marginBottom: '5px',
    textTransform: 'capitalize',
    fontSize: '1.5rem',
  },
}

const IMAGE_STYLE = {
  position: 'absolute',
  width: '100%',
  height: '100%',
  transition: '.3s',
}
