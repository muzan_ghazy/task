import { Box, Typography } from '@mui/material'
type Props = {
  text: string
  textTop?: boolean
}

function LineUI({ text, textTop = false }: Props) {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: textTop ? 'column' : 'column-reverse',
        margin: '20px',
        alignItems: textTop ? 'center' : 'n',
        justifyContent: 'center',
        gap: '15px',
        position: 'relative',
        zIndex: 9,
      }}
    >
      <Typography
        sx={{
          transform: 'rotate(-90deg)',
          width: 'fit-content',
          height: textTop ? 'fit-content' : '110px',
        }}
      >
        {text}
      </Typography>
      <Box
        component="img"
        src="/images/line.png"
        sx={{
          height: '95px',
          width: 'fit-content',
          transform: textTop ? 'rotate(180deg)' : 'rotate(0)',
        }}
      />
    </Box>
  )
}

export default LineUI
