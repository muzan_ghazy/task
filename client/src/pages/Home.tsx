import HeaderSection from '../components/pages/HomePage/HeaderSection'
import AboutSection from '../components/pages/HomePage/AboutSection'
import LineUI from '../components/common/lineUI/LineUI'
import { Box } from '@mui/material'
import ProductsSection from '../components/pages/HomePage/ProductsSection'

function Home() {
  return (
    <>
      <HeaderSection />
      <Box
        sx={{
          margin: 'auto',
          width: 'fit-content',
          position: 'relative',
          zIndex: 2,
        }}
      >
        <Box data-aos="fade-down">
          <LineUI text="Scroll" textTop />
        </Box>
      </Box>
      <AboutSection />
      <ProductsSection />
    </>
  )
}

export default Home
