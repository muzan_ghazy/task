export const NavList: Array<{ title: string; href: string }> = [
  {
    title: 'anasayfa',
    href: '/',
  },
  {
    title: 'hakkimizda',
    href: '/hakkimizda',
  },
  {
    title: 'belgeler',
    href: '/belgeler',
  },
  {
    title: 'esans',
    href: '/esans',
  },
  {
    title: 'aroma',
    href: '/aroma',
  },
]
