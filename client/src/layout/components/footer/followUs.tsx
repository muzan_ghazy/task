import FacebookIcon from '@mui/icons-material/Facebook'
import TwitterIcon from '@mui/icons-material/Twitter'
import LinkedInIcon from '@mui/icons-material/LinkedIn'
import InstagramIcon from '@mui/icons-material/Instagram'
import { Box, Typography, IconButton } from '@mui/material'

function FollowUs() {
  return (
    <>
      <Typography sx={{ fontSize: '1.5rem', fontWeight: 600 }}>
        Follow Us
      </Typography>
      <Box>
        <IconButton color="secondary">
          <FacebookIcon />
        </IconButton>
        <IconButton color="secondary">
          <TwitterIcon />
        </IconButton>
        <IconButton color="secondary">
          <LinkedInIcon />
        </IconButton>
        <IconButton color="secondary">
          <InstagramIcon />
        </IconButton>
      </Box>
    </>
  )
}

export default FollowUs
