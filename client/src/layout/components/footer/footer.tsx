import { Box, Container, Grid } from '@mui/material'
import { NavList } from '../types'
import { useNavigate } from 'react-router-dom'
import FollowUs from './followUs'

function Footer() {
  const navigate = useNavigate()

  return (
    <Box component="footer" sx={{ paddingBottom: '40px' }}>
      <Container>
        <Grid container spacing={2}>
          <Grid item md={6} xs={12}>
            <Box component="img" src="/images/logo.svg" />
          </Grid>
          <Grid container spacing={2} item md={6} xs={12}>
            <Grid item md={4} xs={12}>
              {NavList.slice(0, 3).map((item) => (
                <Box
                  key={item.title}
                  onClick={() => navigate(item.href)}
                  sx={ITEM_STYLES}
                >
                  {item.title}
                </Box>
              ))}
            </Grid>
            <Grid item md={4} xs={12}>
              {NavList.slice(3).map((item) => (
                <Box
                  key={item.title}
                  onClick={() => navigate(item.href)}
                  sx={ITEM_STYLES}
                >
                  {item.title}
                </Box>
              ))}
            </Grid>
            <Grid item md={4} xs={12}>
              <FollowUs />
            </Grid>
          </Grid>
        </Grid>
      </Container>
    </Box>
  )
}

export default Footer

const ITEM_STYLES = {
  cursor: 'pointer',
  textTransform: 'uppercase',
  margin: '10px 0',
  transition: '.3s',
  // fontWeight: 500,
  '&:hover': {
    opacity: 0.7,
  },
}
