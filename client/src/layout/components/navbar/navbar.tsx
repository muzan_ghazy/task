import { Box } from '@mui/material'
import MobileNavbar from './mobileNavbar'
import NavbarContent from './navbarContent'

function Navbar() {
  return (
    <Box sx={NAV_STYLES}>
      <Box
        sx={{
          '@media screen and (max-width:940px)': {
            display: 'none',
          },
        }}
      >
        <NavbarContent />
      </Box>
      <MobileNavbar />
    </Box>
  )
}

export default Navbar

const NAV_STYLES = {
  position: 'fixed',
  top: '0',
  padding: '20px',
  width: '100vw',
  zIndex: 99,
  background: '#d6b04e4d',
}
