import { Box } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import { NavList } from '../types'

function ButtonsList() {
  const navigate = useNavigate()

  return (
    <Box
      component="ul"
      sx={{
        display: 'flex',
        gap: '20px',
        listStyleType: 'none',
        flex: 1,

        '@media screen and (max-width:940px)': {
          flexDirection: 'column',
          marginBottom: '20px',
        },
      }}
    >
      {NavList.map((item, index: number) => (
        <Box
          component="li"
          key={item.title}
          sx={{
            textTransform: 'uppercase',
            cursor: 'pointer',
            transition: '.3s',
            fontWeight: 600,
            '&:hover': { opacity: 0.7 },
            flex: 1,
          }}
          onClick={() => navigate(item.href)}
          data-aos="fade-right"
          data-aos-delay={(index + 5) * 100}
        >
          <Box sx={{ width: 'fit-content' }}>
            {item.title}
            {item.href === '/' ? (
              <Box
                sx={{
                  width: '24px',
                  height: '2px',
                  background: '#000',
                  margin: '10px auto 0',
                }}
              />
            ) : null}
          </Box>
        </Box>
      ))}
    </Box>
  )
}

export default ButtonsList
