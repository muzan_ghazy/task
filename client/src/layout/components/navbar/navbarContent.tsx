import { Box, Button, Container } from '@mui/material'
import ButtonsList from './buttonsList'
import PublicIcon from '@mui/icons-material/Public'
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown'

function NavbarContent() {
  return (
    <>
      <Container sx={CONTAINER_STYLES}>
        <Box
          sx={{
            flex: 1,
            '@media screen and (max-width:940px)': {
              marginBottom: '40px',
            },
          }}
        >
          <Box
            component="img"
            src="/images/logo.svg"
            data-aos="fade-down"
            data-aos-delay="100"
          />
        </Box>

        <ButtonsList />
        <Box sx={BUTTONS_STYLES}>
          <Button
            variant="outlined"
            color="secondary"
            sx={{ borderRadius: '25px' }}
            data-aos="fade-down"
            data-aos-delay="100"
          >
            Contact Us
          </Button>
          <Button
            variant="outlined"
            color="secondary"
            sx={{ borderRadius: '25px', color: '#121212' }}
            endIcon={<ArrowDropDownIcon />}
            data-aos="fade-down"
            data-aos-delay="200"
          >
            <PublicIcon />
          </Button>
        </Box>
      </Container>
    </>
  )
}

export default NavbarContent

const BUTTONS_STYLES = {
  flex: 1,
  justifyContent: 'right',
  display: 'flex',
  gap: '10px',
}

const CONTAINER_STYLES = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  '@media screen and (max-width:940px)': {
    flexDirection: 'column',
    padding: '20px',
  },
}
