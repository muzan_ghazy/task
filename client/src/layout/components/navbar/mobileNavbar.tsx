import { Box, Container } from '@mui/material'
import MenuIcon from '@mui/icons-material/Menu'
import * as React from 'react'
import SwipeableDrawer from '@mui/material/SwipeableDrawer'
import Button from '@mui/material/Button'
import NavbarContent from './navbarContent'

type Anchor = 'top' | 'left' | 'bottom' | 'right'

function MobileNavbar() {
  const anchor = 'right'
  const [state, setState] = React.useState({
    top: false,
    left: false,
    bottom: false,
    right: false,
  })

  const toggleDrawer =
    (anchor: Anchor, open: boolean) =>
    (event: React.KeyboardEvent | React.MouseEvent) => {
      if (
        event &&
        event.type === 'keydown' &&
        ((event as React.KeyboardEvent).key === 'Tab' ||
          (event as React.KeyboardEvent).key === 'Shift')
      ) {
        return
      }

      setState({ ...state, [anchor]: open })
    }

  return (
    <Container sx={CONTAINER_STYLES}>
      <Box sx={{ flex: 1 }}>
        <Box
          component="img"
          src="/images/logo.svg"
          data-aos="fade-down"
          data-aos-delay="100"
        />
      </Box>

      <Box>
        <Button onClick={toggleDrawer(anchor, true)}>
          <MenuIcon sx={{ color: '#121212' }} />
        </Button>
        <SwipeableDrawer
          anchor={anchor}
          open={state[anchor]}
          onClose={toggleDrawer(anchor, false)}
          onOpen={toggleDrawer(anchor, true)}
          sx={{
            '@media screen and (min-width:941px)': {
              display: 'none',
            },
          }}
        >
          <Box>
            <NavbarContent />
          </Box>
        </SwipeableDrawer>
      </Box>
    </Container>
  )
}

export default MobileNavbar

const CONTAINER_STYLES = {
  justifyContent: 'space-between',
  alignItems: 'center',
  display: 'none',
  '@media screen and (max-width:940px)': {
    display: 'flex',
  },
}
