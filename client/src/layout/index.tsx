import { Box } from '@mui/material'
import { Outlet } from 'react-router-dom'
import Navbar from './components/navbar/navbar'
import Footer from './components/footer/footer'
import AOS from 'aos'
import 'aos/dist/aos.css'
import { useEffect } from 'react'

function Layout() {
  useEffect(() => {
    AOS.init({
      duration: 1000,
    })
    AOS.refresh()
  }, [])

  return (
    <Box component="main">
      <Navbar />
      <Outlet />
      <Footer />
    </Box>
  )
}

export default Layout
