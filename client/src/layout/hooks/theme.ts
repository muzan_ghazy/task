import { createTheme } from '@mui/material/styles'
import { grey } from '@mui/material/colors'

const theme = createTheme({
  palette: {
    primary: {
      main: grey[50],
    },
    secondary: {
      main: '#C49F72',
    },
  },
  typography: {
    fontFamily: 'sans',
  },
})

export default theme
