const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const productsRoutes = require('./routes/productRoutes')

const app = express()
const URI =
  'mongodb://127.0.0.1:27017/testTask?directConnection=true&serverSelectionTimeoutMS=2000&appName=mongosh+1.8.0'

app.use(bodyParser.json())

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader(
    'Access-Control-Allow-Methods',
    'OPTIONS, GET, POST, PUT, PUSH, DELETE'
  )
  res.setHeader('Access-Control-Allow-Headers', 'Content-type, Authorization')
  next()
})

app.use(productsRoutes)

app.use((err, req, res, next) => {
  console.log(err)
  res.status(err.statusCode).json({
    message: err.message,
    data: err.data,
  })
})

mongoose
  .connect(URI)
  .then((res) => {
    console.log('Connected!')
    app.listen(8000)
  })
  .catch((err) => {
    console.log(err)
  })
