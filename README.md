# Task App

### `npm install` then `cd client` and `npm i`

In the project directory, you can run server and client together using:

### `npm run dev`

Runs the app in the development mode.\
Open [http://localhost:5173](http://localhost:5173) to view it in the browser.
and [http://localhost:8000](http://localhost:8000) to run server.

### You can adding products using postman:

POST action [https://localhost:8000/product](https://localhost:8000/product)
json body: `{"title":"title test", "desc":"desc test", "imgUrl":"/images/testProduct.svg" }`
